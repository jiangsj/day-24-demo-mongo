/**
 * Created by fliwa on 15/11/16.
 */

/*Simple app to get user input from command line*/

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/fsf3_db');

console.log('Welcome to my node app');


var key = '';
var value = '';

process.argv.forEach(function(val, index, array){
    console.log(index + ': ' + val);
    if(index == 2) {
        key = val;
    }
    if(index == 3) {
        value = val;
    }
});
console.log('inputs: ' + key + ':' + value);

// sample run
// [0] = node
// [1] = app name
// [node,appname,parameters...]
// node input.js name jill



var query = {};

if(key == '_id')
    query[key] = mongoose.Types.ObjectId(value);
else
    query[key] = value;



var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    //console.log('DB connected!');
});


var Schema = mongoose.Schema;

var Pet   = new Schema(
    {
        name:String
    }
)

//Defining Person schema
var PersonSchema  = new Schema({
    name:String,
    pets:[Pet]
})

//Adding methods to your schema
PersonSchema.methods.speak = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting);
}

PersonSchema.methods.shout = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting+ greeting);
}

//               mongoose.model(<collection name>, <schema>)
var PersonModel = mongoose.model('Blogs',PersonSchema);


PersonModel.find(query,function(err,res){

    if(err)
        console.log(err)
    else
    {
        if( res.length != 0)
            console.log('Found! ' + res);
        else
            console.log('Person does not exist!');

    }
})
