/**
 * Created by fliwa on 15/11/16.
 */

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/fsf3_db');


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    //console.log('DB connected!');
});


var Schema = mongoose.Schema;

var Pet   = new Schema(
    {
        name:String
    }
)

//Defining Person schema
var PersonSchema  = new Schema({
    name:String,
    pets:[Pet]
})

//Adding methods to your schema
PersonSchema.methods.speak = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting);
}

PersonSchema.methods.shout = function() {
    var greeting = this.name ? "Hello,my name is "+this.name : "I don't have a name.";
    console.log(greeting+ greeting);
}

//               mongoose.model(<collection name>, <schema>)
var PersonModel = mongoose.model('Blogs',PersonSchema);

//var person = new PersonModel({name:'John'});
var person = new PersonModel({name:'Jill'});

person.pets.push({name:'meow'} );

person.save(function(err){

});

console.log('Person:' + person);

person.speak();
person.shout();





